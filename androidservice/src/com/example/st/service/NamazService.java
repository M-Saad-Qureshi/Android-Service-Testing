package com.example.st.service;

import com.example.st.MainActivity;
import com.example.st.R;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat.Builder;

;

public class NamazService extends IntentService
{

	public NamazService()
	{
		super(NamazService.class.getName());
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent)
	{
		// Normally we would do some work here, like download a file.
		// For our sample, we just sleep for 5 seconds.

		long endTime = System.currentTimeMillis() + 5 * 1000;
		while (System.currentTimeMillis() < endTime)
		{
			synchronized (this)
			{
				try
				{
					wait(endTime - System.currentTimeMillis());
				}
				catch (Exception e)
				{
				}
			}
		}

		Intent resultIntent = new Intent(this, MainActivity.class);

		PendingIntent resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		Intent resultIntent2 = new Intent(this, MainActivity.class);
		resultIntent2.putExtra("arg1", "Icon was clicked");

		PendingIntent resultPendingIntent2 = PendingIntent.getActivity(this, 0, resultIntent2, PendingIntent.FLAG_UPDATE_CURRENT);

		Intent resultIntent3 = new Intent(this, MainActivity.class);
		resultIntent3.putExtra("arg1", "Icon2 was clicked");

		PendingIntent resultPendingIntent3 = PendingIntent.getActivity(this, 0, resultIntent3, PendingIntent.FLAG_UPDATE_CURRENT);

		Builder mBuilder = new Builder(this).setSmallIcon(R.drawable.notification_icon).setContentTitle("My notification")
				.setContentText("Hello World!").addAction(R.drawable.icon, "Call", resultPendingIntent2)
				.addAction(R.drawable.icon, "Message", resultPendingIntent3);
		/*
		 * Here resultPendingIntent3 was added at very last so in device with
		 * Android lower than 4.1, notification will not show any button and
		 * opens the activity with the data which was recently added
		 */

		mBuilder.setContentIntent(resultPendingIntent);

		// Sets an ID for the notification
		int mNotificationId = 001;
		// Gets an instance of the NotificationManager service
		NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		// Builds the notification and issues it.
		mNotifyMgr.notify(mNotificationId, mBuilder.build());

	}
}
